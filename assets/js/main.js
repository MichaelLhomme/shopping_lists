// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
//import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
import socket from "./socket"

import Vue from 'vue'

import VueI18n from 'vue-i18n'
Vue.use(VueI18n)

import store from './store'
import vuetify from './vuetify'
import router from './router'

const i18n = new VueI18n({
	locale: 'fr',
	messages: {
		en: {
		},

		fr: {
		}

	}
})

let v = new Vue({
  el: "#app",
  router,
  store,
  vuetify,
	i18n,
});
