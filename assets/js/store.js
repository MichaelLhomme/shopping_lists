import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    lists: [],
    items: []
  },

  getters: {

    // Get a list by it's id
    getList: (state) => (id) => {
      return state.lists.find(list => list.id === id)
    },

    // Get a item by it's id
    getItem: (state) => (id) => {
      return state.items.find(item => item.id === id)
    },

    // Get a list of items by theirs list's id
    getItems: (state) => (id) => {
      return state.items
    },

  },


  mutations: {

    // Set a new list of shopping lists
    set_lists(state, lists) {
      state.lists = lists
    },

    // Add a new list
    add_list(state, list) {
      state.lists.push(list)
    },

    // Delete a list
    delete_list(state, list) {
      let index = state.lists.findIndex((l) => l.id == list.id)
      if(index >= 0) state.lists.splice(index, 1)
    },

    // Update a list
    update_list(state, list) {
      let index = state.lists.findIndex((l) => l.id == list.id)
      if(index >= 0) Vue.set(state.lists, index, Object.assign({}, state.lists[index], list))
    },


    // Set a new list of shopping lists
    set_items(state, items) {
      state.items = items
    },

    // Add a new item
    add_item(state, item) {
      state.items.push(item)
    },

    // Delete an item
    delete_item(state, item) {
      let index = state.items.findIndex(i => i.id == item.id)
      if(index >= 0) state.items.splice(index, 1)
    },

    // Update an item
    update_item(state, item) {
      let index = state.items.findIndex(i => i.id == item.id)
      if(index >= 0) Vue.set(state.items, index, Object.assign({}, state.items[index], item))
    },

  }
})

export default store
