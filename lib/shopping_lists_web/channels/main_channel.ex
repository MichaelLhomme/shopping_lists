defmodule ShoppingListsWeb.MainChannel do
  use ShoppingListsWeb, :channel

  require Logger
  import Ecto, only: [assoc: 2]
  import Ecto.Query, only: [from: 2]
  alias ShoppingLists.{Repo, List}


  def join("main", payload, socket) do
    if authorized?(payload) do
      lists = Repo.all(from l in List, join: i in assoc(l, :items), preload: [items: i])
      {:ok, lists, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end


  def handle_in("create_list", %{"name" => name, "description" => description}, socket) do
    Logger.debug "Creating list #{name}"

    case Repo.insert(%List{name: name, description: description}) do
      {:ok, list} -> broadcast socket, "list_created", list
      {:error, changeset} -> Logger.error "Error creating list: #{inspect changeset}"
    end

    {:reply, :ok, socket}
  end


  def handle_in("update_list", %{"id" => id} = payload, socket) do
    Logger.debug "Updating list #{id} with #{inspect payload}"

    case Repo.get(List, id) do
      nil -> Logger.warn "Cannot update unknown list #{id}"
      list ->
        list = Ecto.Changeset.change list, name: payload["name"], description: payload["description"]
        case Repo.update(list) do
          {:ok, list} -> broadcast socket, "list_updated", list
          {:error, changeset} -> Logger.error "Error updating list: #{inspect changeset}"
        end
    end

    {:reply, :ok, socket}
  end


  def handle_in("delete_list", %{"id" => id}, socket) do
    Logger.debug "Deleting list #{id}"

    case Repo.get(List, id) do
      nil -> Logger.warn "Cannot delete unknown list #{id}"
      list ->
        Repo.delete(list)
        broadcast socket, "list_deleted", list
    end

    {:reply, :ok, socket}
  end


  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end


defimpl Jason.Encoder, for: ShoppingLists.List do
  def encode(struct, opts) do
    struct
    |> Map.from_struct
    |> Map.update!(:items, fn items ->
      cond do
        is_list(items) -> (Enum.reject(items, fn i -> i.checked end) |> length)
        true -> 0
      end
    end)
    |> Map.take([:id, :name, :description, :items])
    |> Jason.Encode.map(opts)
  end
end
