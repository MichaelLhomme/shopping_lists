defmodule ShoppingLists.Item do
  use Ecto.Schema
  import Ecto.Changeset

  schema "items" do
    field :checked, :boolean, default: false
    field :name, :string
    field :list_id, :integer

    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:name, :checked, :list_id])
    |> validate_required([:name, :list_id])
  end
end
