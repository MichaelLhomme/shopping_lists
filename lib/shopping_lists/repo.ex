defmodule ShoppingLists.Repo do
  use Ecto.Repo,
    otp_app: :shopping_lists,
    adapter: Ecto.Adapters.Postgres
end
