defmodule ShoppingLists.List do
  use Ecto.Schema
  import Ecto.Changeset

  schema "lists" do
    field :description, :string
    field :name, :string
    has_many :items, ShoppingLists.Item

    timestamps()
  end

  @doc false
  def changeset(list, attrs) do
    list
    |> cast(attrs, [:name, :description])
    |> validate_required([:name])
  end
end
